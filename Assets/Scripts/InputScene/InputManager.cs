using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    [SerializeField] private Text final_score;
    private GameMaster gamemaster;
    void Start()
    {
        gamemaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        final_score.text = gamemaster.total_points.ToString();
    }
    
    public void GoToNextScene()
    {
        StartCoroutine(WaitSeconds());
    }


    //Espera unos segundos y carga la siguiente escena
    private IEnumerator WaitSeconds()
    {
        yield return new WaitForSeconds(2.0f);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
