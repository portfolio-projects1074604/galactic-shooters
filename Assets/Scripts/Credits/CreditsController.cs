using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsController : MonoBehaviour
{
    private void Start()
    {
        StartCoroutine(ReturnToStart());
    }

    private IEnumerator ReturnToStart()
    {
        yield return new WaitForSeconds(5.0f);
        SceneManager.LoadScene("StartScreen");
    }
}
