using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreenManager : MonoBehaviour
{
    public GameObject tutorialscreen;
    public void StartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    public void CloseGame()
    {
        Application.Quit();
    }
    public void ShowTutorial()
    {
        tutorialscreen.SetActive(true);
    }
    public void HideTutorial()
    {
        tutorialscreen.SetActive(false);
    }
}
