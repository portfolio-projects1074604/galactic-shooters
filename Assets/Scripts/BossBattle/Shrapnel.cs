using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shrapnel : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f; //Velocidad del proyectil
    [SerializeField] private int damage; //Da�o que causa
    [SerializeField] private Rigidbody2D _rigidbody; //Componente RigidBody2D
    [SerializeField] private int pattern; //Patron en el que se mueve el disparo (0 = en cruz, 1 = en X)
    [SerializeField] private int order; //El orden de generacion del disparo
    // Start is called before the first frame update
    void Start()
    {
        //Obtenemos el componente
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        //Hacemos que el objeto se vaya a destruir pasados unos segundos
        StartCoroutine(DestroyOverTime());        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //Si el patron es el primero...
        if (pattern == 0)
        {
            switch (order) //Segun el proyectil que sea le asignamos una fuerza especifica en cada una de las 4 direcciones
            {
                case 0:
                    _rigidbody.AddForce(Vector2.up * speed);
                    break;
                case 1:
                    _rigidbody.AddForce(Vector2.left * speed);
                    break;
                case 2:
                    _rigidbody.AddForce(Vector2.down * speed);
                    break;
                case 3:
                    _rigidbody.AddForce(Vector2.right * speed);
                    break;
                default:
                    break;
            }
        }
        else if (pattern == 1) //Si el patron es el segundo...
        {
            Vector2 v; //...Creamos un nuevo vector para la direccion
            switch (order)
            {
                //...Y lo creamos en funcion de que proyectil sea, cada uno se mueve por una diagonal
                case 0:
                    v = new Vector2(1, 1);
                    _rigidbody.AddForce(v * speed);
                    break;
                case 1:
                    v = new Vector2(-1, 1);
                    _rigidbody.AddForce(v * speed);
                    break;
                case 2:
                    v = new Vector2(-1, -1);
                    _rigidbody.AddForce(v * speed);
                    break;
                case 3:
                    v = new Vector2(1, -1);
                    _rigidbody.AddForce(v * speed);
                    break;
                default:
                    break;
            }
        }    
    }

    //Metodo para obtener el patron y el orden de cada proyectil generado por la bomba
    public void SetPattern(int pattern, int order)
    {
        this.pattern = pattern;
        this.order = order;
    }

    //Corrutina para que el objeto se destruya pasados unos segundos
    private IEnumerator DestroyOverTime()
    {
        yield return new WaitForSeconds(4.0f);
        Destroy(gameObject);
    }
}
