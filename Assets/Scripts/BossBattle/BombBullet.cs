using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBullet : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;
    [SerializeField] private Rigidbody2D _rigidbody;
    [SerializeField] private GameObject shrapnel;
    [SerializeField] private List<GameObject> points;
    [SerializeField] private int pattern;
    [SerializeField] private int damage;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = gameObject.GetComponent<Rigidbody2D>();
        pattern = Random.Range(0, 2);
        StartCoroutine(ActivateBomb());
    }

    // Update is called once per frame
    void Update()
    {
        _rigidbody.AddForce(Vector2.up * speed);
    }

    public int GetDamage()
    {
        return damage;
    }

    private IEnumerator ActivateBomb()
    {
        yield return new WaitForSeconds(2.0f);
        for (int i = 0; i < 4; i++)
        {
            var bullet = Instantiate(shrapnel, transform.position, points[i].transform.rotation);
            bullet.GetComponent<Shrapnel>().SetPattern(pattern, i);
        }
        Destroy(gameObject);
    }
}
