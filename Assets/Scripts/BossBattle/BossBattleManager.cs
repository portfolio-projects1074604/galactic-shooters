using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BossBattleManager : MonoBehaviour
{
    [SerializeField] private Sprite _doubleShot; //Prefab de la mejora de disparo doble
    [SerializeField] private Sprite _tripleShot; //Prefab de la mejora de disparo triple
    [SerializeField] private Text score; //Puntuacion acumulada por el jugador en texto
    [SerializeField] private Text time; //Timepo de juego
    [SerializeField] private Image powerUParea; //Imagen de la barra de estado del powerup
    [SerializeField] private Image endscreen; //Pantalla de fin de juego
    [SerializeField] private AudioManager audio_manager;
    [SerializeField] private GameMaster gamemaster; //Referencia al game master
    [SerializeField] private GameObject player_ship; //Nave del jugador
    [SerializeField] private GameObject boss; //Nave jefe enemiga
    [SerializeField] private Transform player_position; //Posicion de inicio del jugador para la batalla
    [SerializeField] private Transform boss_position; //Posicion de inicio del jefe enemigo para la batalla
    [SerializeField] private Transform finish_point; //Punto donde el jugador va al acabar la batalla

    private int total_points; //Total de puntos acumulados
    private bool battle_started; //Boleano para comprobar que comienza la batalla
    private bool battle_finished = false; //Boleano para comprobar que la batalla ha terminado
    // Start is called before the first frame update
    void Start()
    {
        //Recuperamos los datos de la escena anterior
        gamemaster = GameObject.Find("GameMaster").GetComponent<GameMaster>();
        total_points = gamemaster.total_points;
        if (gamemaster.powerup.Equals("double_shot"))
        {
            player_ship.GetComponent<ShipController2D>().ActivateDoubleShot();
            powerUParea.sprite = _doubleShot;
        }
        else if (gamemaster.powerup.Equals("triple_shot"))
        {
            player_ship.GetComponent<ShipController2D>().ActivateTripleShot();
            powerUParea.sprite = _tripleShot;
        }

        //Desactivamos en primer lugar los controladores de los participantes en la batalla para poder crear una "cutscene"
        player_ship.GetComponent<ShipController2D>().enabled = false;
        boss.GetComponent<BossController>().enabled = false;
        audio_manager.PlayBossBattleIntro(); //Hacemos sonar la musica introductoria
    }

    // Update is called once per frame
    void Update()
    {
        //En cada frame actualizamos los puntos y el tiempo 
        score.text = "Score: " + total_points;
        time.text = "Time: " + (int)Time.time + "s";
        //Comprobamos que la batalla no haya tenido lugar ya
        if (!battle_started && !battle_finished)
        {
            //Si el jugador no esta en posicion lo lleva hasta alli
            if (player_ship.transform.position != player_position.position)
            {
                player_ship.transform.position = Vector3.MoveTowards(player_ship.transform.position, player_position.position, 2.5f * Time.deltaTime);
            }
            //Si el jefe no esta alli lo lleva hasta alli
            if (boss.transform.position != boss_position.position)
            {
                boss.transform.position = Vector3.MoveTowards(boss.transform.position, boss_position.position, 2.55f * Time.deltaTime);
            }
            //Cuando ambos estan colocados...
            if (player_ship.transform.position == player_position.position && boss.transform.position == boss_position.position)
            {
                //... empezamos la batalla, activando los controladores de cada uno y cambiando la musica por la de la batalla
                battle_started = true;
                player_ship.GetComponent<ShipController2D>().enabled = true;
                boss.GetComponent<BossController>().enabled = true;
                audio_manager.PlayBossBattle();
            }
        }
        //Si la batalla ha terminado...
        else if (battle_finished)
        {
            //... movemos al jugador hasta el punto de fin
            player_ship.transform.position = Vector3.MoveTowards(player_ship.transform.position, finish_point.position, Time.fixedDeltaTime);
            gamemaster.SavePoints(total_points);
            if (player_ship.transform.position == finish_point.position)
            {
                endscreen.gameObject.SetActive(true);
            }
        }
    }

    //Metodo para sumar los puntos obtenidos al total
    public void AddPoints(int points)
    {
        total_points += points;
    }
    //Metodo para finalizar la batalla
    public void FinishBattle()
    {
        battle_finished = true; //ponemos el boleano a true
        player_ship.GetComponent<ShipController2D>().enabled = false; //desactivamos el controlador para que el jugador no pueda moverse
        player_ship.GetComponent<CircleCollider2D>().isTrigger = true; //hacemos que el collider sea un trigger para que algun disparo que quede por ahi no pueda destruir la nave
    }

    //Metodo para pasar a la pantalla de resultados
    public void GoToInputScreen()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
