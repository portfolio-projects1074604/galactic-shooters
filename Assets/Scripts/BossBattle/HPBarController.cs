using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HPBarController : MonoBehaviour
{
    [SerializeField] private int health; //Cantidad de vida de la barra
    [SerializeField] private SpriteRenderer healthBar; //Barra de vida del jefe
    private Vector3 healthScale; //Escala de la barra de vida
    // Start is called before the first frame update
    void Start()
    {
        healthScale = healthBar.transform.localScale; //Obtenemos la escala de la barra de vida
    }
    
    public void UpdateHealthBar(int actual_health)
    {
        if (healthBar != null)
        {
            health = actual_health;
            // Ponemos el color de la barra de vida proporcional a la vida del enemigo entre verde y rojo
            healthBar.color = Color.Lerp(Color.green, Color.red, 1 - health * 0.003f);

            // Ponemos la escala de la barra de vida proporcional a la vida del jugador
            healthBar.transform.localScale = new Vector3(healthScale.x * health * 0.01f, healthScale.y, 1);
        }      
    }
}
