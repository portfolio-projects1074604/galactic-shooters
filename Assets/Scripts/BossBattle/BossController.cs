using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    private BossBattleManager _bm; //Referencia al BossBattleManager
    private Transform _player; //Transform del jugador
    private Animator animator; //Controlador de animaciones
    private AudioManager audio_manager; //Clase que contiene la pistas de audio


    public GameObject _bullet; //Prefab de la bala que dispara
    public GameObject _bomb; //Prefab de la bomba que dispara
    public List<GameObject> cannons; //Lista de ca�ones, lugares donde se crean los disparon
    public HPBarController healthBar;

    //Bordes del movimiento del jefe
    [SerializeField] private Transform left_edge; 
    [SerializeField] private Transform right_edge;


    [SerializeField] private float speed = 3.0f; //Velocidad de movimiento del enemigo
    [SerializeField] private int health; //Vida del enemigo, desaparece al llegar a 0
    private bool canShoot = true; //Boleano que indica si el enemigo puede disparar o no
    private bool canShootBomb = true; //Boleano que indica si el enemigo puede disparar bombas o no
    private bool turn = false;
    private int direction = 2; //Direccion en la que se mueve la nave en cada momento

    // Start is called before the first frame update
    void Start()
    {
        //Obtenemos los componentes necesarios, en este caso debe hacerse en el Start porque las instancias se crean en Runtime
        _bm = GameObject.Find("BossBattleManager").GetComponent<BossBattleManager>();
        _player = GameObject.Find("PlayerShip").GetComponent<Transform>();
        animator = gameObject.GetComponent<Animator>();
        audio_manager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
    }

    private void FixedUpdate()
    {
        if (_player != null) //Comprobamos que tengamos la referencia al jugador
        {
            //Si puede disparar, crea una nueva instancia de disparo en cada ca�on que tenga la nave
            if (canShoot)
            {
                foreach (GameObject cannon in cannons)
                {
                    Instantiate(_bullet, cannon.transform.position, transform.rotation);
                }
                //Al terminar inicia una corrutina para que espere unos segundo para volver a disparar
                StartCoroutine(ShootCooldown());
            }
            //Mismo proceso para las bombas
            if (canShootBomb)
            {
                Instantiate(_bomb, cannons[1].transform.position, transform.rotation);
                StartCoroutine(BombCooldown());
            }
            //Comprobamos si podemos elegir una nueva direccion. Si podemos elegimos entre izquierda y derecha y comenzamos el refresco
            if (!turn)
            {
                direction = Random.Range(0, 2);
                StartCoroutine(TurnCooldown()); //Corrutina para temporizar la eleccion de direccion
            }
            if (direction == 0) //Si vamos hacia la derecha
            {
                //La nave se mueve hacia el borde derecho a una velocidad constante multiplicada por el tiempo
                transform.position = Vector3.MoveTowards(transform.position, right_edge.position, Time.fixedDeltaTime * speed);
            }
            else if(direction == 1) //Si vamos hacia la izquierda
            {
                //La nave se mueve hacia el borde izquierdo a una velocidad constante multiplicada por el tiempo
                transform.position = Vector3.MoveTowards(transform.position, left_edge.position, Time.fixedDeltaTime * speed);
            }
            //Si la nave llega hasta unos de los bordes cambiamos forzosamente la direcc�on
            if (transform.position == right_edge.position)
            {
                direction = 1;
            }
            if (transform.position == left_edge.position)
            {
                direction = 0;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si la nave enemiga choca con la bala del jugador...
        if (collision.CompareTag("Bullet"))
        {
            //... le reducimos la vida en la cantidad de da�o que haga el disparo, destruimos la bala...
            health -= collision.gameObject.GetComponent<PlayerBullet>().GetDamage();
            Destroy(collision.gameObject);
            if (health > 0)
            {
                healthBar.UpdateHealthBar(health); //Actualizamos la barra de vida del jefe
            }          
            //... y despues, si le hemos bajado a 0 la vida, eliminamos la nave
            if (health == 0)
            {
                audio_manager.PlayEnemyExplosion(); //animaci�n de destruccion del enemigo
                StartCoroutine(WaitToKill()); //Corrutina que hace esperar al script hasta que la animaci�n termine
                _bm.AddPoints(5000); //Sumamos puntos al jugador al derrotar al enemigo
            }
        }
    }

    //Corrutinas para que los enemigos no puedan disparar continuamente
    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(3.0f);
        canShoot = true;
    }

    private IEnumerator BombCooldown()
    {
        canShootBomb = false;
        yield return new WaitForSeconds(10.0f);
        canShootBomb = true;
    }

    //Corrutina para que el objeto no se destruya antes de que termine su animaci�n de muerte
    private IEnumerator WaitToKill()
    {
        animator.SetBool("Dead", true); 
        yield return new WaitForSeconds(0.8f);
        _bm.FinishBattle(); //Indicamos al gestor de la batalla que el jefe ha sido derrotado
        Destroy(gameObject);
    }

    //Corrutina para controlar el cambio de direccion
    private IEnumerator TurnCooldown()
    {
        turn = true;
        yield return new WaitForSeconds(3.0f);
        turn = false;
    }
}
