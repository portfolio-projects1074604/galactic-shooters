using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameMaster : MonoBehaviour
{
    private static GameMaster instance;
    public int total_points;
    public string powerup;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(instance);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SavePoints(int points)
    {
        total_points = points;
    }
    public void SavePowerup(string pu)
    {
        powerup = pu;
    }
}
