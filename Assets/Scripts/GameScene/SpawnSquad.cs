using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnSquad : MonoBehaviour
{
    [SerializeField] private float _enemy_count;
    [SerializeField] private List<GameObject> _spawn_points;
    [SerializeField] private GameObject _enemy_ship;
    [SerializeField] private AudioSource enemie_alert;
    private bool used = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !used)
        {
            Debug.Log("Spawn");
            enemie_alert.Play();
            foreach (GameObject spawn in _spawn_points)
            {
                Instantiate(_enemy_ship, spawn.transform.position, spawn.transform.rotation);
            }
            used = true;
        }
    }

}
