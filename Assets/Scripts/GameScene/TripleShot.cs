using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TripleShot : MonoBehaviour
{
    public GameplayManager gm;
    public GameObject powerup_sprite;
    public Sprite powerup;
    [SerializeField] private ParticleSystem particles;
    [SerializeField] private AudioManager audio_manager;

    [SerializeField] private float spawn_time;
    [SerializeField] private float warning_time;

    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
        particles = GameObject.Find("PowerUpParticles").GetComponent<ParticleSystem>();
        audio_manager = GameObject.Find("AudioManager").GetComponent<AudioManager>();
        StartCoroutine(DeSpawn());
    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > warning_time)
        {
            var main = particles.main;
            main.startColor = new ParticleSystem.MinMaxGradient(Color.red);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            gm.PutPOwerUPinUI(powerup);
            gm.AddPoints(1000);
            audio_manager.PlayTripleShot();
            collision.gameObject.GetComponent<ShipController2D>().ActivateTripleShot();
            Destroy(gameObject);
        }
    }


    private IEnumerator DeSpawn()
    {
        yield return new WaitForSeconds(spawn_time);
        Destroy(gameObject);
    }
}
