using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBullet : MonoBehaviour
{
    private float speed = 30f; //Velocidad de movimiento de la bala
    [SerializeField] private int damage; //Da�o que la bala causa a los enemigos
    private Rigidbody2D _rigdbody; //Componente RigidBody2D de la bala
    // Start is called before the first frame update
    void Start()
    {
        _rigdbody = gameObject.GetComponent<Rigidbody2D>(); //Obtenemos el RigidBody
        StartCoroutine(DestroyOverTime());// Comenzamos la corrutina 
    }

    private void FixedUpdate()
    {
        //Comprobamos la rotaci�n de la bala
        if (transform.rotation.z % 360 == 0)
        {
            _rigdbody.AddForce(Vector2.down * speed); //Si la rotaci�n es igual a 0 le a�adimos fuerza hacia abajo a la bala
        }
        else
        {
            _rigdbody.AddForce(Vector2.up * speed); //Si no lo es la nave estara dada la vuelta y le daremos fuerza hacia arriba
        }
        
    }
    //Metodo para obtener el da�o que hace el disparo
    public int GetDamage()
    {
        return damage;
    }
    //Corrutina que, pasados unos segundo, destruye el objeto para que los disparos fallidos no existan eternamente
    private IEnumerator DestroyOverTime()
    {
        yield return new WaitForSeconds(3.0f);
        Destroy(gameObject);
    }
}
