using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
    [SerializeField] private GameObject _doubleShotPrefab; //Prefab de la mejora de disparo doble
    [SerializeField] private GameObject _tripleShotPrefab; //Prefab de la mejora de disparo triple
    [SerializeField] private GameObject player_ship; //Nave del jugador
    [SerializeField] private Transform finish_point; //Punto donde se cambia la escena
    [SerializeField] private GameMaster gamemaster; //Referencia al GameMaster para pasar informacion entre escenas

    [SerializeField] private Image powerUParea; //Imagen de la barra de estado del powerup
    [SerializeField] private Text score; //Puntuacion acumulada por el jugador en texto
    [SerializeField] private Text time; //Timepo de juego
    private int total_points; //Total de puntos acumulados
    private bool finished_level = false;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //En cada frame actualizamos los puntos y el tiempo 
        score.text = "Score: " + total_points;
        time.text = "Time: " + (int)Time.time + "s";
        gamemaster.SavePoints(total_points);
        if (finished_level)
        {
            Debug.Log("Entro update");
            player_ship.GetComponent<ShipController2D>().enabled = false; //desactivamos el controlador para que el jugador no pueda moverse
            player_ship.GetComponent<CircleCollider2D>().isTrigger = true; //hacemos que el collider sea un trigger para que algun disparo que quede por ahi no pueda destruir la nave
            //... movemos al jugador hasta el punto de fin
            player_ship.transform.position = Vector3.MoveTowards(player_ship.transform.position, finish_point.position, Time.deltaTime);
            if (player_ship.transform.position == finish_point.position)
            {
                //Cargamos la siguiente escena al finalizar el nivel
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
        }
    }

    //Metodo para sumar los puntos obtenidos al total
    public void AddPoints(int points)
    {
        total_points += points;
    }
     //Metodo para generar una mejora al derrotar a un enemigo
    public void SpawnPowerUp(Transform enemy)
    {
        float type = Random.Range(0.0f, 1.0f); //Obtenemos un valor aleatorio
        if (type < 0.2f) //Si ese valor es menor que 0.2 generamos una mejora de disparo triple
        {
            Instantiate(_tripleShotPrefab, enemy.position, Quaternion.identity);
        }
        else if (type < 1.0f) //Si ese valor es menor que 1 generamos un disparo doble
        {
            Instantiate(_doubleShotPrefab, enemy.position, Quaternion.identity);
        }
    }

    //Metodo para colocar en la interfaz el powerup que hayamos conseguido
    public void PutPOwerUPinUI(Sprite powerup)
    {
        powerUParea.sprite = powerup;
    }

    public void FinishedLevel(bool state)
    {
        finished_level = state;
        AddPoints(200 / (int) Time.time);
    }
}
