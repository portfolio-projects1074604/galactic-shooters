using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour
{
    [SerializeField] private float speed = 5.0f;
    private Transform _player;
    private Vector3 _player_position;
    private Quaternion _player_rotation;
    // Start is called before the first frame update
    void Start()
    {
        _player = GameObject.Find("PlayerShip").GetComponent<Transform>();
        _player_position = _player.transform.position;
        _player_rotation = _player.transform.rotation;
        StartCoroutine(DestroyOverTime());
    }

    private void FixedUpdate()
    {
        /* Para misiles teledirigidos ekisde
        float step = speed * Time.fixedDeltaTime;
        transform.position = Vector3.MoveTowards(transform.position, _player.transform.position, step);*/

        float step = speed * Time.fixedDeltaTime;
        transform.SetPositionAndRotation(Vector3.MoveTowards(transform.position, _player_position, step), Quaternion.RotateTowards(transform.rotation, _player_rotation, step));
        if (transform.position == _player_position)
        {
            Destroy(gameObject);
        }
    }


    private IEnumerator DestroyOverTime()
    {
        yield return new WaitForSeconds(3.0f);
        Destroy(gameObject);
    }
}
