using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private GameplayManager _gm; //Referencia al GameplayManager
    private Transform _player; //Transform del jugador
    private Animator animator; //Controlador de animaciones
    private AudioManager audio_manager; //Clase que contiene la pistas de audio

    public GameObject _bullet; //Prefab de la bala que dispara
    public GameObject cannon; //Referencia al ca�on por donde dispara la nave


    [SerializeField] private float speed = 3.0f; //Velocidad de movimiento del enemigo
    [SerializeField] private float _minDistance = 8.0f; //Minimo de distancia a la que el enemigo tiene que estar para poder disparar al jugador
    [SerializeField] private int health; //Vida del enemigo, desaparece al llegar a 0
    private bool canShoot = true; //Boleano que indica si el enemigo puede disparar o no

    // Start is called before the first frame update
    void Start()
    {
        //Obtenemos los componentes necesarios, en este caso debe hacerse en el Start porque las instancias se crean en Runtime
        _gm = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
        _gm = GameObject.Find("GameplayManager").GetComponent<GameplayManager>();
        _player = GameObject.Find("PlayerShip").GetComponent<Transform>();
        animator = gameObject.GetComponent<Animator>();
        audio_manager = GameObject.Find("AudioManager").GetComponent<AudioManager>();

    }

    private void FixedUpdate()
    {
        if (_player != null) //Comprobamos que tengamos la referencia al jugador
        {
            //Obtenemos la distancia entre el jugador y el enemigo, asi como el paso al que se va a acercar al jugador...
            float distance = Vector3.Distance(_player.position, transform.position);
            float distance_step = speed * Time.fixedDeltaTime;
            //... si esta distancia es mayor que la que habiamos fijado...
            if (distance > _minDistance)
            {        
                //... movemos la nave enemiga en direccion al jugador lo que marque el step
                transform.position = Vector3.MoveTowards(transform.position, _player.position, distance_step);
            }
            else
            {
                //Si puede disparar, crea una nueva instancia de disparo en cada ca�on que tenga la nave
                if (canShoot)
                {
                    Instantiate(_bullet, cannon.transform.position, transform.rotation);
                    StartCoroutine(ShootCooldown()); //Al terminar inicia una corrutina para que espere unos segundo para volver a disparar
                }
            }
        }              
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si la nave enemiga choca con la bala del jugador...
        if (collision.CompareTag("Bullet"))
        {
            //... le reducimos la vida en la cantidad de da�o que haga el disparo, destruimos la bala...
            health -= collision.gameObject.GetComponent<PlayerBullet>().GetDamage();
            Destroy(collision.gameObject);
            //... y despues, si le hemos bajado a 0 la vida, eliminamos la nave
            if (health == 0)
            {
                audio_manager.PlayEnemyExplosion(); //animaci�n de destruccion del enemigo
                StartCoroutine(WaitToKill()); //Corrutina que hace esperar al script hasta que la animaci�n termine
                _gm.AddPoints(100); //Sumamos puntos al jugador al derrotar al enemigo
                //Hacemos aparecer una mejora segun una probabilidad
                if (Random.Range(0.0f, 10.0f) > 0.2f)
                {
                    _gm.SpawnPowerUp(gameObject.transform); //Metodo del GameplayManager para que se generen mejoras al derrotar enemigos
                }
            } 
        }
    }

    //Corrutinas para que los enemigos no puedan disparar continuamente
    private IEnumerator ShootCooldown()
    {
        canShoot = false;
        yield return new WaitForSeconds(3.0f);
        canShoot = true;
    }

    //Corrutina para que el objeto no se destruya antes de que termine su animaci�n de muerte
    private IEnumerator WaitToKill()
    {
        gameObject.GetComponent<CapsuleCollider2D>().enabled = false;
        animator.SetBool("Dead", true);
        yield return new WaitForSeconds(1.0f);
        Destroy(gameObject);    
    }
}
