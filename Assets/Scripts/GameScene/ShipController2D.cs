using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipController2D : MonoBehaviour
{
    [Range(0, .3f)] [SerializeField] private float m_MovementSmoothing = .05f;  // Cuanto suavizamos el movimiento

    private Vector3 m_Velocity = Vector3.zero; //Velocidad de la nave

    public Rigidbody2D m_Rigidbody; //RigidBody2D de la nave
    public GameObject shot; //Prefabs del disparo de la nave del jugador
    public GameObject cannon; //Ca�on principal de la nave
    private Animator animator; //Controlador de animaciones
    public GameplayManager _gm;
    public GameMaster master;
    public List<GameObject> double_cannon; //Lista con los ca�ones que usa la nave en caso de tener activo el disparo doble
    public List<GameObject> triple_cannons; //Lista con los ca�ones que usa la nave en caso de tener activo el disparo triple

    public int speed; //Velocidad de la nave
    private float move = 0f; //Axis en la que nos movemos
    private bool cooldown = false; //Boleano para el refresco de los disparos
    private bool turn = false; //Boleano que indica si la nave esta orientada hacia arriba o abajo

    [SerializeField] private bool double_shot = false; //Boleano para controlar el disparo doble
    [SerializeField] private bool triple_shot = false; //Boleano para controlar el disparo triple
    private AudioManager audio_manager; //Referencia al gestor de pistas de audio
    // Start is called before the first frame update
    void Start()
    {
        master = GameObject.Find("GameMaster").GetComponent<GameMaster>(); //Obtenemos el game master
        audio_manager = GameObject.Find("AudioManager").GetComponent<AudioManager>(); //Obtenemos el AudioManager
        animator = gameObject.GetComponent<Animator>(); //Obtenemos el animador
    }

    private void FixedUpdate()
    {
        //Si pulsamos algun boton del eje vertical...
        if (Input.GetButton("Vertical"))
        {
            //Obtenemos el movimiento con el eje multiplicado por la velocidad a la que nos movemos...
            move = Input.GetAxisRaw("Vertical") * speed;
            //...y obtenemos esa velocidad como un vector dependiente del tiempo y lo suavizamos con SmoothDamp
            Vector3 targetVelocity = new Vector2(m_Rigidbody.velocity.x, move * 10f) * Time.fixedDeltaTime; //Mantenemos el movimiento horizontal y usamos el nuevo movimiento vertical
            m_Rigidbody.velocity = Vector3.SmoothDamp(m_Rigidbody.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing);
        }
        //Si el eje es horizontal haremos lo mismo pero esta vez cambiaremos el movimiento horizontal
        if (Input.GetButton("Horizontal"))
        {
            move = Input.GetAxisRaw("Horizontal") * speed;
            Vector3 targetVelocity = new Vector2(move * 10f, m_Rigidbody.velocity.y) * Time.fixedDeltaTime;
            m_Rigidbody.velocity = Vector3.SmoothDamp(m_Rigidbody.velocity, targetVelocity, ref m_Velocity, m_MovementSmoothing) ;
        }
        //Si pulsamos el boton de disparo...
        if (Input.GetButton("Shoot"))
        {
            //...y no estamos en periodo de refresco...
            if (!cooldown)
            {
                //podremos disparar el funcion de que modo de disparo tengamos
                //Si tenemos el disparo doble...
                if (double_shot)
                {
                    //... se crean dos instancias de balas, cada una en uno de los ca�ones conrrespondientes
                    Instantiate(shot, double_cannon[0].transform.position, transform.rotation);
                    Instantiate(shot, double_cannon[1].transform.position, transform.rotation);
                    audio_manager.PlayLaserShot(); //Despues hacemos sonar el efecto de disparo
                    StartCoroutine(ShootCooldown()); //y comienza la rutina de refresco
                }
                //Si tenemos el disparo triple...
                else if (triple_shot)
                {
                    //... se crean tre instancias de balas, cada una en uno de los ca�ones conrrespondientes
                    Instantiate(shot, triple_cannons[0].transform.position, transform.rotation);
                    Instantiate(shot, triple_cannons[1].transform.position, transform.rotation);
                    Instantiate(shot, triple_cannons[2].transform.position, transform.rotation);
                    audio_manager.PlayLaserShot(); //Despues hacemos sonar el efecto de disparo
                    StartCoroutine(ShootCooldown()); //y comienza la rutina de refresco
                }
                //Si no tenemmos ninguna mejora...
                else
                {
                    //... se crea una unica instancia en el ca�on principal
                    Instantiate(shot, cannon.transform.position, transform.rotation);
                    audio_manager.PlayLaserShot(); //Despues hacemos sonar el efecto de disparo
                    StartCoroutine(ShootCooldown()); //y comienza la rutina de refresco
                }
            }
        }
        //Si pulsamos el boton de giro...
        if (Input.GetButton("Turn"))
        {
            //comprobamos que podamos girar, porque sin limitarlo el giro el demasiado sensible
            if (!turn)
            { 
                //Rotamos la nave 180 grados para que mire en direccion contraria
                transform.Rotate(Vector3.forward, 180f);
                StartCoroutine(TurnCooldown()); //y comenzamos el refresco del giro
            }         
        }
    }
    //Metodos para activar distintas mejoras al obtenerlas
    public void ActivateDoubleShot()
    {
        DeactivatePowerUps();
        master.SavePowerup("double_shot");
        double_shot = true;
    }
    public void ActivateTripleShot()
    {
        DeactivatePowerUps();
        master.SavePowerup("triple_shot");
        triple_shot = true;      
    }

    //Metodo para desactivar los powerups que no tengamos, se usa al obtener uno nuevo para asegurar que no tengaos 2 al mismo tiempo
    private void DeactivatePowerUps()
    {
        double_shot = triple_shot = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Si chocamos con un enemigo (tanto naves como disparos)...
        if (collision.CompareTag("Enemy"))
        {
            Destroy(collision.gameObject);
            audio_manager.PlayPlayerExplosion(); //empieza la animaccion de derrota
            //animator.Play("PlayerShip");
            StartCoroutine(WaitToKill());
        }
        if (collision.CompareTag("end"))
        {
            _gm.FinishedLevel(true);
        }
    }
    //Corrutinas para que no se pueda disparar continuamente
    private IEnumerator ShootCooldown()
    {
        cooldown = true;
        yield return new WaitForSeconds(0.3f);
        cooldown = false;
    }

    //Corrutinas para que no se pueda girar continuamente
    private IEnumerator TurnCooldown()
    {
        turn = true;
        yield return new WaitForSeconds(0.1f);
        turn = false;
    }

    //Corrutina para que el objeto no se destruya antes de que termine su animaci�n de muerte
    private IEnumerator WaitToKill()
    {
        animator.SetBool("Dead", true);
        yield return new WaitForSeconds(0.7f);
        Destroy(gameObject); //Destruimos la nave al terminar la animacion
    }
}
