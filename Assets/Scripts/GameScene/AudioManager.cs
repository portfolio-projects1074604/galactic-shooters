using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioSource enemy_explosion;
    [SerializeField] private AudioSource player_explosion;
    [SerializeField] private AudioSource lasershot;
    [SerializeField] private AudioSource double_shot;
    [SerializeField] private AudioSource triple_shot;
    [SerializeField] private AudioSource boss_battle;
    [SerializeField] private AudioSource boss_battle_ost;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    

    public void PlayEnemyExplosion()
    {
        enemy_explosion.Play();
    }

    public void PlayPlayerExplosion()
    {
        player_explosion.Play();
    }
    public void PlayLaserShot()
    {
        lasershot.Play();
    }
    public void PlayDoubleShot()
    {
        double_shot.Play();
    }
    public void PlayTripleShot()
    {
        triple_shot.Play();
    }
    public void PlayBossBattleIntro()
    {
        boss_battle_ost.Play();
    }
    public void PlayBossBattle()
    {
        boss_battle_ost.Stop();
        boss_battle.Play();
    }

}
