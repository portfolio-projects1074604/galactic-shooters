# GALACTIC SHOOTERS



## INTRODUCCION

Galactic Shooters es un juego de naves que trata de emular a los antiguos juegos de arcade del mismo tipo.

Este proyecto consta de una primera fase donde se tendrá que derrotar a los enemigos que vayan apareciendo, y después de una segunda fase donde habrá un enfrentamiento contra un jefe. El objetivo será tanto superar el recorrido como obtener la máxima puntuación posible.

## MECANICAS 

En Galactic Shooters se recorre la fase de arriba abajo automáticamente. El jugador puede mover la nave en cualquier dirección dentro del espacio que muestra la cámara, y perderá si sale de la zona. También es posible cambiar el sentido en el que apunta la nave.
Las naves enemigas irán apareciendo en oleadas y se deberá luchar contra elllas. Para ello, el jugador dispone de armas en la nave con las que disparar a los enemigos. 
Si la salud del jugador baja a 0 este pierde y debe reiniciar el nivel.

Al ser derrotados, los enemigos tienen una probabilidad de dejar caer distintas mejoras para la nave. En esta versión, se pueden conseguir tanto el disparo doble como el triple. Además de la mejora, también se dan unos puntos al cogerla.

## SCREENSHOTS

![Nave del jugador](images/Nave%20del%20jugador.png)

![Nave enemiga](images/Nave%20enemiga.png)

![Jefe](images/Jefe.png)
